'use strict'

// modules
const { convertObj } = require('swagger2openapi')
const through = require('through2')
const gutil = require('gulp-util')

// constants
const PLUGIN_NAME = 'gulp-swagger2openapi'
const PluginError = gutil.PluginError

/**
 * @function
 * @name swagger2openapi
 */
module.exports = function(options = {}) {
  return through.obj(function(file, enc, callback) {
    if (file !== null && file.isBuffer()) {
      // isBuffer
      let json

      try {
        json = JSON.parse(file.contents.toString())
      } catch (err) {
        throw err
      }
      convertObj(json, options)
        // convert success
        .then(({ openapi }) => {
          const newFile = new gutil.File()
          const data = JSON.stringify(openapi)

          newFile.path = 'open-api.json'
          newFile.contents = Buffer.from(data)
          callback(null, newFile)
        })

        // convert failed
        .catch(err => {
          this.emit('error', new PluginError(PLUGIN_NAME, err.message))
          callback()
        })
    } else {
      this.emit(
        'error',
        new PluginError(PLUGIN_NAME, 'Only Buffer format is supported')
      )
      callback()
    }
  })
}
